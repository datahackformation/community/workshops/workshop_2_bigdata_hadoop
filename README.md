# Workshop: Aplicaciones de Big Data con Hadoop

![img](banner.jpg)

En este repositorio compartimos el material del evento:

* Presentación 

* Guía para instalar una máquina virtual de Cloudera

* El video está en nuestro [canal de Youtube](https://www.youtube.com/watch?v=hrUh_FdbtJw&feature=youtu.be). 

**Comunidad Data Hack Formation**
Los materiales de anteriores workshops son libres para toda la comunidad en [GitLab](https://gitlab.com/datahackformation).

Además tenemos una publicación en [Medium](https://medium.com/disruptiva) con artículos y tutoriales gratuitos de Big Data y Cloud.

## Agradecimiento
Agradecemos al docente [Jimmy Farfán](https://www.linkedin.com/in/jimmy-farfan-mba/) por la exposición.

## FAQ
Cualquier duda o consulta pueden hacerlo a nuestro facebook [Data Hack Formation](https://fb.com/datahackformation).